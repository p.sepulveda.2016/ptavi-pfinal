#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time
import socket
import socketserver
import hashlib
import os
import threading


class XML_file(ContentHandler):
    """ Class Handler para obtener datos de archivo XML """

    def __init__(self):
        """ Inicializo diccionario """
        self.diccionario = {'account': ['username', 'passwd'],
                        'uaserver': ['ip', 'puerto'],
                        'rtpaudio': ['puerto'],
                        'regproxy': ['ip', 'puerto'],
                        'log': ['path'],
                        'audio': ['path']}
        self.dic_ua = {}

    def startElement(self, item, attrs):
        """ Añado el diccionario a una lista """
        if item in self.diccionario:
            for atributo in self.diccionario[item]:
                self.dic_ua[item + '_' + atributo] = attrs.get(atributo, '')

    def get_tags(self):
        return self.dic_ua


def log(evento, file):
    """ Creo el fichero log y escribo en él los eventos. """
    time_actual = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    file = open(file, 'a')
    file.write(time_actual + '  ' + evento + '\r\n')
    file.close()


def rtp(ip, port, audio):
    """Manda Audio RTP."""
    # aEjecutar es un string que se pasa a una subshell
    aejecutar = './mp32rtp -i ' + ip + ' -p ' + port + ' < ' + audio
    cvlc = 'cvlc rtp://@' + ip + ':' + port
    hcvlc = threading.Thread(target=os.system(cvlc + '&'))
    hmp3 = threading.Thread(target=os.system(aejecutar))
    hcvlc.start()
    hmp3.start()
    return cvlc + aejecutar


def request():
        # Funcion para crear las peticiones del cliente.
    if METHOD == 'REGISTER':
        try:
            TIME_EXPIRES = int(OPTION)
            REQUEST = ('REGISTER sip:' + USER + ':'
                        + PORT_SERVER + ' SIP/2.0\r\n'
                        + 'Expires: ' + str(TIME_EXPIRES))
        except ValueError:
            sys.exit("For the REGISTER method option must be a integer.")
    elif METHOD == 'INVITE':
        REQUEST = ('INVITE sip:' + OPTION + ' SIP/2.0\r\n'
                    + 'Content-Type: application/sdp\r\n\r\n'
                    + 'v=0\r\n' + 'o=' + USER + ' ' + IP_SERVER
                    + '\r\n' + 's=misesion\r\n'
                    + 't=0\r\n' + 'm=audio ' + PORT_RTP + ' RTP')
    elif METHOD == 'BYE':
            REQUEST = ('BYE sip:' + USER + ':' + PORT_SERVER + ' SIP/2.0')
    print('Envio petición al proxy: \r\n' + REQUEST + '\r\n')
    log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY) + ': ' +
       REQUEST, LOG_FILE)
    my_socket.send(bytes(REQUEST, 'utf-8') + b'\r\n')


def reply():
    # Funcion respuestas del cliente.
    RESPONSE = data_str.split(' ')
    if RESPONSE[6] == '401':
        key = hashlib.md5()
        nonce = ((data_str.split(' ')[10]).split('"')[1])
        key.update(bytes(PASSWORD, 'utf-8'))
        key.update(bytes(nonce, 'utf-8'))
        new_key = key.hexdigest()
        TIME_EXPIRES = int(OPTION)
        REQUEST = ('REGISTER sip:' + USER + ':' + PORT_SERVER + ' SIP/2.0\r\n'
                   + 'Expires: ' + str(TIME_EXPIRES) + '\r\n')
        ANSWER = (REQUEST + 'Authorization: Digest response='
                  + new_key + '\r\n')
        print('Respuesta del cliente: \r\n' + ANSWER)
        log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY) + ': ' +
           ANSWER, LOG_FILE)
        my_socket.send(bytes(ANSWER, 'utf-8') + b'\r\n')
    elif RESPONSE[1] == '100' and RESPONSE[3] == '180' and RESPONSE[5] == '200':
        ANSWER = 'ACK sip:' + OPTION + ' SIP/2.0\r\n'
        print('Respuesta del cliente: \r\n' + ANSWER)
        log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY) + ': ' +
           ANSWER, LOG_FILE)
        my_socket.send(bytes(ANSWER, 'utf-8') + b'\r\n')
        rtp(IP_SERVER, PORT_RTP, AUDIO_FILE)
    elif RESPONSE[1] == '200':
        pass
        log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY) + ': ' +
           RESPONSE, LOG_FILE)
    else:
        pass


if __name__ == "__main__":
    try:
        CONFIG = sys.argv[1]              # Archivo XML con la configuración
        METHOD = sys.argv[2]              # Método
        OPTION = sys.argv[3]              # Expires/Receptor
    except IndexError:
        sys.exit("Usage: python3 uaclient.py config method option")
        """ $ python uaclient.py ua1.xml INVITE penny@girlnextdoor.com """

        # Guardo el diccionario self.dic_ua en XML.
    parser = make_parser()
    cHandler = XML_file()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONFIG))
    XML = cHandler.get_tags()

    # Variables ua1.xml (servidor local)
    USER = XML['account_username']
    PASSWORD = XML['account_passwd']
    IP_PROXY = XML['regproxy_ip']
    PORT_PROXY = int(XML['regproxy_puerto'])
    PORT_SERVER = XML['uaserver_puerto']
    IP_SERVER = XML['uaserver_ip']
    PORT_RTP = XML['rtpaudio_puerto']
    AUDIO_FILE = XML['audio_path']
    LOG_FILE = XML['log_path']

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((IP_PROXY, PORT_PROXY))
        request()

        # Recibe respuestas por parte de proxy_registrar
        try:
            data = my_socket.recv(1024)
            data_str = data.decode('utf-8')
            if data_str == '':
                data_str = 'SIP/2.0 200 OK'
            # Solucion para la respuesta vacía del BYE
            print('Respuesta del proxy: \r\n' + data_str)
            if data_str == 'SIP/2.0 200 OK':
                pass
            else:
                reply()
        except ConnectionRefusedError:
            sys.exit("Conexion fallida")
