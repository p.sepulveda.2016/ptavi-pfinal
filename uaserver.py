#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socketserver
import os
from uaclient import XML_file, rtp, log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

try:
    CONFIG = sys.argv[1]        # Archivo XML de configuración
except (IndexError, ValueError):
    sys.exit("Usage: python3 uaserver.py config")


class Server(socketserver.DatagramRequestHandler):
    """  Server class  """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            new_line = line.decode('utf-8').split(" ")
            """
            Arreglo de fallo, recibe new_line ['']
            """
            if not line and len(new_line):
                break       # Si no hay más lineas del bucle infinito
            method_sip = new_line[5].split('\r\n')[1]
            if method_sip == 'INVITE':
                USER_CLIENT = new_line[8].split('=')[2]
                print("El proxy nos envía: \r\n\r\n" + line.decode('utf-8'))
                answer = ("SIP/2.0 100 Trying\r\n\r\n"
                            + "SIP/2.0 180 Ringing\r\n\r\n"
                            + "SIP/2.0 200 OK\r\n\r\n")
                new_answer = (answer + 'Content-Type: application/sdp\r\n\r\n' +
                           'v=0\r\n' + 'o=' + USER_CLIENT + ' ' + IP_SERVER + '\r\n' +
                           's=misesion\r\n' + 'm=audio ' + str(PORT_RTP) +
                           ' RTP' + '\r\n\r\n')
                self.wfile.write(bytes(new_answer, 'utf-8'))
                print('Enviamos al Proxy: \r\n\r\n', new_answer)
                log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY) + ': ' +
                   new_answer, LOG_FILE)
            elif method_sip == 'BYE':
                print("El cliente nos envía:\r\n" + line.decode('utf-8') + '\r\n')
                print('Enviamos al Proxy:\r\nSIP/2.0 200 OK\r\n\r\n')
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY)
                    + ': SIP/2.0 200 OK\r\n\r\n', LOG_FILE)
            elif method_sip == 'ACK':
                print('El proxy nos envía:\r\n' + line.decode('utf-8'))
                rtp(IP_SERVER, PORT_RTP, AUDIO_FILE)
                log('Sent from ' + IP_PROXY + ':' + str(PORT_PROXY)
                    + line.decode('utf-8'), LOG_FILE)
            elif method_sip != ('INVITE' or 'BYE' or 'ACK'):
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
                log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY)
                    + ' ERROR: SIP/2.0 405 Method Not Allowed\r\n', LOG_FILE)
            elif len(method_sip) < 2:
                pass
            else:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                log('Sent to ' + IP_PROXY + ':' + str(PORT_PROXY)
                    + ' ERROR: SIP/2.0 400 Bad Request\r\n', LOG_FILE)


if __name__ == "__main__":

    parser = make_parser()
    cHandler = XML_file()
    parser.setContentHandler(cHandler)
    parser.parse(open(CONFIG))
    XML = cHandler.get_tags()

        # Variables ua1.xml (servidor local)
    USER_SERVER = XML['account_username']
    PASSWORD = XML['account_passwd']
    IP_PROXY = XML['regproxy_ip']
    PORT_PROXY = int(XML['regproxy_puerto'])
    PORT_SERVER = int(XML['uaserver_puerto'])
    IP_SERVER = XML['uaserver_ip']
    PORT_RTP = XML['rtpaudio_puerto']
    AUDIO_FILE = XML['audio_path']
    LOG_FILE = XML['log_path']

    """ Creamos el servidor de eco y esucuchamos """
    serv = socketserver.UDPServer((IP_SERVER, PORT_SERVER), Server)
    print("Listening...\r\n")
    log('Server starting to listening...', LOG_FILE)
    try:
        " Creamos el servidor "
        serv.serve_forever()
    except KeyboardInterrupt:           # Interrumpo servidor por teclado
        print("Finalizado servidor")
        log('Finishing server...', LOG_FILE)
