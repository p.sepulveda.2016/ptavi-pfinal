#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socketserver
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaclient import XML_file, log
import json
import hashlib
import random
import time
import socket


class XMLpr_file(ContentHandler):
    """ Clase para obtener datos del archivo XML del proxy. """

    def __init__(self):
        self.diccionario = {'server': ['name', 'ip', 'puerto'],
                            'database': ['path', 'passwdpath'],
                            'log': ['path']}
        self.dic_ua = {}

    def startElement(self, item, attrs):
        if item in self.diccionario:
            for atributo in self.diccionario[item]:
                self.dic_ua[item + '_' + atributo] = attrs.get(atributo, '')

    def get_tags(self):
        return self.dic_ua


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Clase del proxy_registrar. """

    dic_passwords = {}
    dic_registered = {}
    nonce = {}

    def json_password(self):
        """ Guardo fichero json de contraseñas en dic_passwords. """
        try:
            with open(PASSWD_USERS, 'r') as jsonfile:
                self.dic_passwords = json.load(jsonfile)
        except (ValueError, FileNotFoundError):
            self.dic_passwords = {}

    def json_register(self):
        """
        Guardo, en caso de que haya usuarios registrados,
        estos en el diccionario.
        """
        try:
            with open(REGISTER, 'r') as jsonfile:
                self.dic_registered = json.load(jsonfile)
        except (ValueError, FileNotFoundError):
            self.dic_registered = {}

    def register_json(self):
        """
        Escribir diccionario.
        En formato json en elfichero registered.json.
        """
        with open(REGISTER, 'w') as jsonfile:
            json.dump(self.dic_registered, jsonfile, indent=4)

    def invite(self, line_str):
            # Respuesta a INVITE.
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            o = (line_str.split('\r\n')[4]).split('=')[1]
            user_client = o.split(' ')[0]
            user_server = (line_str.split(' ')[1]).split(':')[1]
            # user_client origen INVITE, user_server destino INVITE
            if user_client in self.dic_registered:
                if user_server in self.dic_registered:
                    ip_server = self.dic_registered[user_server]['ip']
                    port_server = int(self.dic_registered[user_server]['puerto'])
                    """
                    Notas para entender mejor
                    el programa
                    """
                    print('****** El servidor es: ' + user_server + ' ' + '[' + ip_server
                            + ' , ' + str(port_server) + ']')
                    print('****** El cliente es: ' + user_client + '\r\n')
                    cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                    cabecera_proxy += str(PORT_SERVER) + '\r\n'
                    line_str = cabecera_proxy + line_str
                    try:
                        sock.connect((ip_server, port_server))
                        sock.send(bytes(line_str, 'utf-8'))
                        log('Sent to ' + IP_SERVER + ':' + str(PORT_SERVER) + ': '
                            + line_str + '\r\n', LOG_FILE)
                        rcv = sock.recv(1024).decode('utf-8')
                        print('El servidor nos envía: \r\n' + rcv)
                    except(ConnectionRefusedError, KeyError):
                        recv = ''
                        cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                        cabecera_proxy += str(PORT_SERVER) + '\r\n'
                        ANSWER = 'SIP/2.0 400 Bad Request\r\n\r\n'
                        cabecera_proxy += ANSWER
                        log('Sent to ' + IP_SERVER + ':' + str(PORT_SERVER) + ' ERROR: '
                            + cabecera_proxy + '\r\n', LOG_FILE)
                        self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
                else:
                    cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                    cabecera_proxy += str(PORT_SERVER) + '\r\n'
                    ANSWER = "SIP/2.0 404 User not found" + "\r\n\r\n"
                    cabecera_proxy += ANSWER
                    log('Sent to ' + IP_SERVER + ':' + str(PORT_SERVER) + ' ERROR: '
                        + cabecera_proxy + '\r\n', LOG_FILE)
                    self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
            else:
                cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                cabecera_proxy += str(PORT_SERVER) + '\r\n'
                ANSWER = "SIP/2.0 404 User not found" + "\r\n\r\n"
                cabecera_proxy += ANSWER
                log('Sent to ' + IP_SERVER + ':' + str(PORT_SERVER) + ' ERROR: '
                    + cabecera_proxy + '\r\n', LOG_FILE)
                self.wfile.write(bytes(cabecera_proxy, 'utf-8'))

    def ack(self, line_str):
            # Funcion que envia ACK recibido por el cliente al servidor
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            user = (line_str.split(' ')[1]).split(':')[1]
            if user in self.dic_registered:
                ip_server = self.dic_registered[user]['ip']
                port_server = int(self.dic_registered[user]['puerto'])
                cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                cabecera_proxy += str(PORT_SERVER) + '\r\n'
                line_str = cabecera_proxy + line_str
                sock.connect((ip_server, port_server))
                sock.send(bytes(line_str, 'utf-8'))
                log('Sent to ' + IP_SERVER + ':' + str(PORT_SERVER) + ' : '
                    + cabecera_proxy + '\r\n', LOG_FILE)
            else:
                cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                cabecera_proxy += str(PORT_SERVER) + '\r\n'
                ANSWER = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                cabecera_proxy += ANSWER
                log('Sent to ' + IP_SERVER + ':' + str(PORT_SERVER) + ' ERROR: '
                    + cabecera_proxy + '\r\n', LOG_FILE)
                self.wfile.write(bytes(cabecera_proxy, 'utf-8'))

    def bye(self, line_str):
            # Funcion que responde al BYE del cliente
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            user = (line_str.split(' ')[1]).split(':')[1]
            if user in self.dic_registered:
                ip_server = self.dic_registered[user]['ip']
                port_server = int(self.dic_registered[user]['puerto'])
                cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                cabecera_proxy += str(PORT_SERVER) + '\r\n'
                line_str = cabecera_proxy + line_str
                try:
                    sock.connect((ip_server, port_server))
                    sock.send(bytes(line_str, 'utf-8'))
                    log('Sent to ' + ip_server + ':' + str(port_server) + ' : '
                        + cabecera_proxy + '\r\n', LOG_FILE)
                    rcv = sock.recv(1024).decode('utf-8')
                    print('Respuesta del servidor: \r\n' + rcv)
                except(ConnectionRefusedError, KeyError):
                    recv = ''
                    cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                    cabecera_proxy += str(PORT_SERVER) + '\r\n'
                    ANSWER = 'SIP/2.0 400 Bad Request\r\n\r\n'
                    cabecera_proxy += ANSWER
                    log('Sent to ' + ip_server + ':' + str(port_server) + ' ERROR: '
                        + cabecera_proxy + '\r\n', LOG_FILE)
                    self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
            else:
                cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                cabecera_proxy += str(PORT_SERVER) + '\r\n'
                ANSWER = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                cabecera_proxy += ANSWER
                log('Sent to ' + ip_server + ':' + str(port_server) + ' ERROR: '
                    + cabecera_proxy + '\r\n', LOG_FILE)
                self.wfile.write(bytes(cabecera_proxy, 'utf-8'))

    def handle(self):
        # Funcion principal
        ip_client = str(self.client_address[0])
        port_client = str(self.client_address[1])
        self.json_register()
        self.json_password()

        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            if not line:
                break       # Si no hay mas lineas sale del bucle infinito
            line_str = line.decode('utf-8')
            print('El cliente nos envía: \r\n' + line_str)
            user = (line_str.split(' ')[1]).split(':')[1]
            method_sip = line_str.split(' ')[0]
            if method_sip == 'REGISTER':
                    # Compruebo que el usuario esté guardado en el fichero de contraseñas
                if user in self.dic_passwords:
                    # Compruebo si está registrado o no
                    if user not in self.dic_registered:
                        time_expires = (line_str.split('\r\n')[1]).split(' ')[1]
                        user_port = (line_str.split(' ')[1]).split(':')[2]
                        if len(line_str.split(' ')) <= 5:
                            """ Cuando len<=5, primera peticion REGISTER sin cabecera nonce. """

                            self.nonce[user] = str(random.randint(0, 999999999))
                            ANSWER = ('SIP/2.0 401 Unauthorized\r\n' +
                                          'WWW Authenticate: Digest ' +
                                          'nonce="' + self.nonce[user] + '" \r\n')
                            cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                            cabecera_proxy += str(PORT_SERVER) + '\r\n'
                            cabecera_proxy += ANSWER
                            self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
                            log('Sent to ' + ip_client + ':' + str(port_client) + ': ' + cabecera_proxy, LOG_FILE)
                            print(cabecera_proxy)
                        else:
                            """ Cuando len=>5, segunda peticion REGISTER con cabecera nonce. """
                            time_actual = time.time()
                            new_expires = time_actual + int(time_expires)
                            key_recv = (line_str.split('\r\n')[2]).split('=')[1]
                            PASSWORD = self.dic_passwords[user]['passwd']
                            key = hashlib.md5()
                            key.update(bytes(PASSWORD, 'utf-8'))
                            key.update(bytes(self.nonce[user], 'utf-8'))
                            new_key = key.hexdigest()
                            if new_key == key_recv:
                                self.dic_registered[user] = {'ip': ip_client,
                                                   'puerto': user_port,
                                                   'tiempo actual': time_actual,
                                                   'tiempo de expiracion': new_expires}
                                cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                                cabecera_proxy += str(PORT_SERVER) + '\r\n'
                                ANSWER = ('SIP/2.0  200  OK \r\n\r\n')
                                cabecera_proxy += ANSWER
                                self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
                                log('Sent to ' + ip_client + ':' + str(port_client) + ': '
                                    + cabecera_proxy + '\r\n', LOG_FILE)
                                print(cabecera_proxy)
                                self.register_json()
                    else:
                        time_expires = (line_str.split('\r\n')[1]).split(' ')[1]
                        if time_expires == '0':
                            del self.dic_registered[user]
                            cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                            cabecera_proxy += str(PORT_SERVER) + '\r\n'
                            ANSWER = ('SIP/2.0  200  OK. Deleting.. \r\n\r\n')
                            cabecera_proxy += ANSWER
                            log('Send: ' + user + ':' + str(port_client)
                                + ': ' + cabecera_proxy + '\r\n', LOG_FILE)
                            print(cabecera_proxy)
                            self.register_json()
                        else:
                            cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                            cabecera_proxy += str(PORT_SERVER) + '\r\n'
                            ANSWER = ('SIP/2.0 User already registered.\r\n\r\n')
                            cabecera_proxy += ANSWER
                            log('Sent to ' + ip_client + ':' + str(port_client) + ': '
                                + cabecera_proxy + '\r\n', LOG_FILE)
                            print(cabecera_proxy)
                            self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
                else:
                    cabecera_proxy = 'Via Proxy IP: ' + IP_SERVER + ' PORT: '
                    cabecera_proxy += str(PORT_SERVER) + '\r\n'
                    ANSWER = 'SIP/2.0 404 User Not Found.\r\n\r\n'
                    cabecera_proxy += ANSWER
                    log('Sent to ' + ip_client + ':' + str(port_client) + ' ERROR: '
                        + cabecera_proxy + '\r\n', LOG_FILE)
                    self.wfile.write(bytes(cabecera_proxy, 'utf-8'))
            elif method_sip == 'INVITE':
                self.wfile.write(b"SIP/2.0 100 Trying\r\n\r\n"
                                 + b"SIP/2.0 180 Ringing\r\n\r\n"
                                 + b"SIP/2.0 200 OK\r\n\r\n")
                self.invite(line_str)
            elif method_sip == 'BYE':
                self.bye(line_str)
            elif method_sip == 'ACK':
                self.ack(line_str)
            elif method_sip != ('INVITE' or 'BYE' or 'ACK'):
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


if __name__ == "__main__":
    try:
        XML = sys.argv[1]        # Archivo XML de configuración
    except (IndexError, ValueError):
        sys.exit("Usage: python3 proxy_registrar.py config")

    # Hago parser para guardar el self.dic_ua en CONFIG.
    parser = make_parser()
    cHandler = XMLpr_file()
    parser.setContentHandler(cHandler)
    parser.parse(open(XML))
    CONFIG = cHandler.get_tags()

    # Variables pr.xml (servidor del proxy)
    USER_SERVER = CONFIG['server_name']
    IP_SERVER = CONFIG['server_ip']
    PORT_SERVER = int(CONFIG['server_puerto'])
    PASSWD_USERS = CONFIG['database_passwdpath']
    REGISTER = CONFIG['database_path']
    LOG_FILE = CONFIG['log_path']

    serv = socketserver.UDPServer((IP_SERVER, PORT_SERVER), SIPRegisterHandler)
    print("Server " + USER_SERVER + " listening at port " + str(PORT_SERVER) + '...' + '\r\n')
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finishing.")
